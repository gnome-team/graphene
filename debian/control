Source: graphene
Section: libs
Priority: optional
Maintainer: Debian GNOME Maintainers <pkg-gnome-maintainers@lists.alioth.debian.org>
Uploaders: Jeremy Bicha <jbicha@ubuntu.com>,
           Laurent Bigonville <bigon@debian.org>
Build-Depends: debhelper-compat (= 13),
               dh-sequence-gir,
               dh-sequence-python3,
               gir1.2-gobject-2.0-dev,
               gobject-introspection,
               gobject-introspection (>= 1.78.1-9~) <cross>,
               libglib2.0-dev,
               meson,
               pkgconf
Build-Depends-Indep: gtk-doc-tools <!nodoc>, libglib2.0-doc <!nodoc>
Rules-Requires-Root: no
Standards-Version: 4.7.0
Vcs-Browser: https://salsa.debian.org/gnome-team/graphene
Vcs-Git: https://salsa.debian.org/gnome-team/graphene.git
Homepage: https://ebassi.github.io/graphene/

Package: libgraphene-1.0-0
Architecture: any
Depends: ${misc:Depends}, ${shlibs:Depends}
Multi-Arch: same
Description: library of graphic data types
 Graphene provides a small set of mathematical types needed to implement
 graphic libraries that deal with 2D and 3D transformations and projections.
 .
 This library provides types and their relative API; it does not deal with
 windowing system surfaces, drawing, scene graphs, or input.

Package: libgraphene-1.0-dev
Section: libdevel
Architecture: any
Multi-Arch: same
Depends: gir1.2-graphene-1.0 (= ${binary:Version}),
         libglib2.0-dev,
         libgraphene-1.0-0 (= ${binary:Version}),
         ${gir:Depends},
         ${misc:Depends}
Provides: ${gir:Provides}
Suggests: libgraphene-doc
Description: library of graphic data types (development files)
 Graphene provides a small set of mathematical types needed to implement
 graphic libraries that deal with 2D and 3D transformations and projections.
 .
 This library provides types and their relative API; it does not deal with
 windowing system surfaces, drawing, scene graphs, or input.
 .
 This package contains the header files required for developing software that
 uses libgraphene.

Package: libgraphene-doc
Section: doc
Architecture: all
Multi-Arch: foreign
Depends: ${misc:Depends}
Build-Profiles: <!nodoc>
Description: library of graphic data types (documentation)
 Graphene provides a small set of mathematical types needed to implement
 graphic libraries that deal with 2D and 3D transformations and projections.
 .
 This library provides types and their relative API; it does not deal with
 windowing system surfaces, drawing, scene graphs, or input.
 .
 This package contains the API documentation.

Package: graphene-tests
Architecture: any
Depends: ${misc:Depends}, ${python3:Depends}, ${shlibs:Depends}
Suggests: gnome-desktop-testing
Description: library of graphic data types (installed tests)
 Graphene provides a small set of mathematical types needed to implement
 graphic libraries that deal with 2D and 3D transformations and projections.
 .
 This library provides types and their relative API; it does not deal with
 windowing system surfaces, drawing, scene graphs, or input.
 .
 This package contains test programs, designed to be run as part of a
 regression testsuite.

Package: gir1.2-graphene-1.0
Section: introspection
Architecture: any
Multi-Arch: same
Depends: ${gir:Depends}, ${misc:Depends}
Description: library of graphic data types (introspection files)
 Graphene provides a small set of mathematical types needed to implement
 graphic libraries that deal with 2D and 3D transformations and projections.
 .
 This library provides types and their relative API; it does not deal with
 windowing system surfaces, drawing, scene graphs, or input.
 .
 This package can be used by other packages using the GIRepository format to
 generate dynamic bindings.
